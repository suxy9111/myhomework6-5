package com.lagou.service.impl;

import com.lagou.dao.RoleMapper;
import com.lagou.domain.*;
import com.lagou.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;


    @Override
    public List<Role> findAllRole(Role role) {
        List<Role> allRole = roleMapper.findAllRole(role);
        return allRole;
    }

    @Override
    public List<Integer> findMenuByRoleId(Integer roleid) {
        List<Integer> menuByRoleId = roleMapper.findMenuByRoleId(roleid);

        return menuByRoleId;
    }

    @Override
    public void roleContextMenu(RoleMenuVo roleMenuVo) {

        //1. 清空中间表的关联关系
        roleMapper.deleteRoleContextMenu(roleMenuVo.getRoleId());

        //2. 为角色分配菜单

        for (Integer mid : roleMenuVo.getMenuIdList()) {

            Role_menu_relation role_menu_relation = new Role_menu_relation();
            role_menu_relation.setMenuId(mid);
            role_menu_relation.setRoleId(roleMenuVo.getRoleId());

            //封装数据
            Date date = new Date();
            role_menu_relation.setCreatedTime(date);
            role_menu_relation.setUpdatedTime(date);

            role_menu_relation.setCreatedBy("system");
            role_menu_relation.setUpdatedby("system");


            roleMapper.roleContextMenu(role_menu_relation);
        }

    }

    @Override
    public void deleteRole(Integer roleid) {

        // 调用根据roleid清空中间表关联关系
        roleMapper.deleteRoleContextMenu(roleid);

        roleMapper.deleteRole(roleid);
    }

    @Override
    public List<ResourceCategory> findResourceListByRoleId(int roleId) {

        //1. 查询当前角色的资源分类
        List<ResourceCategory> resourceCategoryList = roleMapper.findResourceCategoryByRoleId(roleId);
        //2. 查询当前角色的资源信息
        List<Resource> resourceList = roleMapper.findResourceListByRoleId(roleId);

        //3. 把资源信息组装在资源分类里
        for (ResourceCategory resourceCategory : resourceCategoryList) {
            ArrayList<Resource> tempList = new ArrayList<>();
            for (Resource resource : resourceList) {
                if(resourceCategory.getId().equals(resource.getCategoryId())){
                    tempList.add(resource);
                }
            }
            resourceCategory.setResourceList(tempList);
        }

        return resourceCategoryList;
    }

    @Override
    public void roleContextResource(RoleResourceVo roleResourceVo) {
        //1. 删除当前角色的资源中间表
        Integer roleId = roleResourceVo.getRoleId();
        roleMapper.deleteRoleResourceRelation(roleId);

//        int t = 2/0;

        //2. 新增记录
        List<Integer> resourceIdList = roleResourceVo.getResourceIdList();
        for (Integer resourceId : resourceIdList) {
            RoleResourceRelation roleResourceRelation = new RoleResourceRelation();
            roleResourceRelation.setResourceId(resourceId);
            roleResourceRelation.setRoleId(roleId);
            Date date = new Date();
            roleResourceRelation.setCreatedTime(date);
            roleResourceRelation.setUpdatedTime(date);
            roleResourceRelation.setCreatedBy("system");
            roleResourceRelation.setUpdatedBy("system");
            roleMapper.insertRoleResourceRelation(roleResourceRelation);
        }
    }

}
