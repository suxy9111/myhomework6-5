package com.lagou.domain;

import java.util.List;

/**
 * @author suxy
 * @date 2020/12/7 19:31
 * @description
 */
public class RoleResourceVo {
    private Integer roleId;
    private List<Integer> resourceIdList;

    @Override
    public String toString() {
        return "RoleResourceVo{" +
                "roleId=" + roleId +
                ", resourceIdList=" + resourceIdList +
                '}';
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public List<Integer> getResourceIdList() {
        return resourceIdList;
    }

    public void setResourceIdList(List<Integer> resourceIdList) {
        this.resourceIdList = resourceIdList;
    }
}
