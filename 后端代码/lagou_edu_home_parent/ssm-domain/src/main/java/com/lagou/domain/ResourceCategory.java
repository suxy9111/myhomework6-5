package com.lagou.domain;

import java.util.Date;
import java.util.List;

/**
 * @author suxy
 * @date 2020/12/7 14:57
 * @description 资源分类表
 * id` int(11) NOT NULL AUTO_INCREMENT,
 *   `name` varchar(200) DEFAULT NULL COMMENT '分类名称',
 *   `sort` int(4) DEFAULT NULL COMMENT '排序',
 *   `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 *   `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
 *   `created_by` varchar(100) NOT NULL COMMENT '创建人',
 *   `updated_by` varchar(100) NOT NULL COMMENT '更新人',
 */
public class ResourceCategory {
    //id，主键
    private Integer id;
    //分类名称
    private String name;
    //排序
    private Integer sort;
    //创建时间
    private Date createdTime;
    //更新时间
    private Date updatedTime;
    //创建人
    private String createdBy;
    //更新人
    private String updatedBy;

    private List<Resource> resourceList;

    @Override
    public String toString() {
        return "ResourceCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sort=" + sort +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", resourceList=" + resourceList +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<Resource> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<Resource> resourceList) {
        this.resourceList = resourceList;
    }
}
